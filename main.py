"""
This is the "front door" of your application which gets submitted
to Spark and serves as the starting point. If you needed to
implement a command-line inteface, for example, you'd invoke the
setup from here.
"""

from __future__ import print_function
from pyspark.sql import SparkSession
from application.util import metaphone_udf

if __name__ == '__main__':
    print("===================                Sample v1 [start!]     ===================")

    spark = (
        SparkSession.builder
            .getOrCreate())
    spark.sparkContext.setLogLevel('WARN')

    print("===================                Sample v1 [working!]     ===================")
    names = (
        spark.createDataFrame(
            data=[
                ('Nick',),
                ('John',),
                ('Frank',),
            ],
            schema=['name']
        ))

    names.select('name', metaphone_udf('name')).show()

    print("===================                Sample v1 [done!]     ===================")


