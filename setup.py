from setuptools import setup

setup(name='metaphone',
      version='0.0.1',
      description='A sample PySpark application',
      packages=['application'],
      zip_safe=False)